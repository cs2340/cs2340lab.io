---
layout: homework
title: CS 2340 Summer 2019 Berlin Project
---

# Berlinkennenlernen (Getting to know Berlin)

You will create a web application for crowdsourcing the process of getting to know a city, in our case Berlin!

## Product Vision

Getting to know a city is fun and daunting.  If you have the good fortune to be part of a group of people who are also getting to know a city, then you can *crowdsource* the exploration process -- you can benefit from other explorers' experience.  If there are many of those fellow explorers, you may even be able to automatically learn from them.

## Software Requirements

### Definitions, Acronyms, and Abbreviations

- **User:** a single user of the web application who is getting to know Berlin.
- **Administrator:** a single user of the web application who enters the static content for the attraction pages. 
- **Attraction:** a place within the city that's worth visiting.
- **Attraction Page:** a page containing information about an attraction, including user reviews.
- **Review:** a textual description of a user's experience with an attraction, which includes a rating on a 5-point scale from 1 star to 5 stars and free-form text.

### User Characteristics

Users of the system will be comfortable using interactive browser-based software, be able to read English language text, and understand simple diagrams and pictures as might be found in popular magazines or newspapers.

### Constraints

-  The system must be implemented on a server running on the JVM.
-  The server must provide a web service API.
-  There must be a client application that runs in a web browser.

#### Berlin Attractions

Your application must contain pages for at least 10 attractions in Berlin.  In addition to the requirements listed elsewhere in this document, each page must contain an attraction summary with historical and cultural information about the attraction.  These summaries should be approximately one page in length and go into more detail than a typical TripAdvisor or Yelp summary.  The goal of this software system is to help visitors to Berlin get to know the city to a deeper level than a typical tourist.

**Required attractions:**

- Berlin Wall
- Templehof Airfield
- Reichstag
- Brandenburg Gate
- Holocaust Memorial

**Additional attractions:**  Your software must have pages for at least 10 attractions in Berlin.  You may choose any attractions you want beyond the required attractions listed above.  Here are a few suggestions:

- Sachsenhausen
- DDR Museum
- Museum Island
    
    - Pergamon Museum
    - Neues Museum
    
- Berlin Cathedral
- Die Hackeschen Höfe

### Milestones

#### <a name="m1"/>M1 Epic: User accounts

User Stories:

- (10 pts) Clean code
- (25 pts) S1: As a *user*, I want to create an account with email and password so that I can log in. 
- (25 pts) S2: As a *user*, I want to maintain a profile in the system with name, birth year, hometown, and interests so that other users can put my reviews in context and the system might give me automatic recommendations. 
- (25 pts) S3: As a *user*, I want to log in to the system so that I can use it in a personalized manner.
- (25 pts) S4: As an *administrator*, I want my user account to be specially marked as an administrator account so that I can perform administrative functions for the web application.
- (5 pts) M1 tag present in [integration manager](https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows)'s repo

#### <a name="m2"/>M2 Epic: Content Pages

- (10 pts) Clean code
- (25 pts) S4: As an *administrator*, I want to create and edit the pages containing information on attractions.
- (50 pts) S5: As a *user* I want to see information information pages for 10 attractions in Berlin. 
- (25 pts) S6: The 
- (5 pts) M1 tag present in [integration manager](https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows)'s repo

#### <a name="m3"/>M3 Epic: User Reviews

- (10 pts) Clean code
- (25 pts) S7: As a *user*, I want to write a review for an attraction so that I can record my experience for others to see.  
- (25 pts) S8: As a *user*, I want to see the reviews written by other users for an attraction so that I can plan my exploration of Berlin more effectively.  
- (25 pts) S9: As an *administrator*, I want to be able to delete a review so that I can enforce rules of conduct.
- (25 pts) S10: As a *user*, I want to be able to edit a review I have already written so that I can update it with new information (like a return visit). 
- (10 pts) Unit tests
- (5 pts) M1 tag present in [integration manager](https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows)'s repo

#### <a name="m4"/>M4 Epic: Cloud Deployment

- (10 pts) Clean code
- (50 pts) S11: As a *devops engineer*, I want to bundle the web application with its dependencies so that deployment is easier. 
- (50 pts) S12: As a *devops engineer*, I want to deploy the application to a cloud platform so that I don't have to maintain servers myself. 
- (5 pts) M1 tag present in [integration manager](https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows)'s repo


