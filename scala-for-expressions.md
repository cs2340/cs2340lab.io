% Scala `for` Expressions

## `for` Loops Versus `for` Expressions

First, we need to be clear on the distinction between `for` loops and `for` expressions.

A for loop is analogous to an imperative loop execited for its effect -- there is no `yield`ing of values.
```Scala
val list = List("now", "is", "", "the", "time")
for (element <- list) {
  println(element)
}
```

A `for` expression `yield`s values.

```Scala
for (element <- list) yield {
  element.toUpperCase
}
```

## `for` Expressions in Depth

A `for` expression is actually syntactic sugar for

- `map`,
- `flatMap`, and
- `withFilter`.

As well as pattern matching.  So to understand `for` expressions, we need to understand these underlying higher-order methods on [`Traversable`](https://www.scala-lang.org/api/current/scala/collection/Traversable.html).

## `map`


Here's the signature of [`map`](https://www.scala-lang.org/api/current/scala/collection/Traversable.html#map[B](f:A=%3EB):Traversable[B]) from the 

```Scala
trait Traversable[+A] {
  def map[B](f: (A) ⇒ B): Traversable[B]
}
```

(That's right, you can use the Unicode double arrow glyph in Scala source.  I recommend you stick with the basic ASCII character set, e.g., =>.)

The `map` method creates a new collection by applying the function argument to each element of the collection on which `map` is called:

```Scala
list.map(e ⇒ e.toUpperCase) // List(NOW, IS, "", THE, TIME)
list.map(e => e.length)     // List(3, 2, 0, 3, 4)
```

Note that while we're using `List`s for simplicity, the higher-order methods on `Traversable` work equally well for `Set`s and `Map`s.

## `flatMap`

Here's the signature of [`flatMap`](https://www.scala-lang.org/api/current/scala/collection/Traversable.html#flatMap[B](f:A=%3Escala.collection.GenTraversableOnce[B]):Traversable[B])

```Scala
def flatMap[B](f: (A) ⇒ GenTraversableOnce[B]): Traversable[B]
``` 

The key difference from `map` is the return type of the function parameter -- a `Traversable`.  These traversables are "flattened" to single elements of type `B` in the result.

If we `map` over our list of `String`s a function that returns a collection, we get:
```Scala
list.map(e => e.toCharArray) // List(Array(n, o, w), Array(i, s), Array(), Array(t, h, e), Array(t, i, m, e))
```

`flatMap` flattens the elements of the sublists into the enclosing list.

```Scala
list.flatMap(e => e.toCharArray) // List(n, o, w, i, s, t, h, e, t, i, m, e)
```

