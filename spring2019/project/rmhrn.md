---
layout: homework
title: CS2340 Spring 2019 Project
---

# Refugee Mental Health Resource Network

Spring 2019 Project

## Purpose

To empower agency represenatatives to connect migrants in need of mental health services with providers of mental health services.

## Roles

- Agency Representative
    - State Department
    - Border Patrol
    - Law firms representing immigrants and refugees
    
- Providers
    - MD Psychiatrists
    - Psychologists (PhD or PsyD)
    - Licensed Clinical Social Workers (LCSW -- US)
    - LPC/Clinical Mental Health Counselor (US)
    - Marriage and Family Therapists (US)
    - Nurse Practitioners (? -- US)
    - Researchers
    - Students
    - Administrators
    
## User Stories

### Agency Rep

- Find a provider
    - Get migrant info
    - Search for providers
    - Contact providers (messaging system)
    - Select provider
    - Receive record of provided care
    - Record feedback on care
    
### Provider

- Register self in system
- Manage own profile
- Respond to requests from agency reps
- Record provided care

### Administrators

- Approve registrations
    - Vettign is external to system -- human process


