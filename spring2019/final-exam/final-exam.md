---
layout: homework
title: Final Exam
---

# Final Exam

## True or False (1 point each)

1. In a distributed version control system with an optimistic locking/merging model, when one develper checks out a file no other developers may edit the file.
2. In a centralized version control system with a pessimistic locking/merging model, when one developer checks out a file no other developers may edit the file.
3. The path of a Java source file from the source root must match the package name of the Java file.
4. `.jar` files contain archives of directory trees that contain `.class` files and other files. 
5. In agile software development, detailed system design is done at the beginning of the project.
6. Agile software development emphasizes comprehensive documentation, processes, and tools.
7. Using CS terms in a variable name is a good idea.
8. It's poor coding practice to, as a rule, comment every function and variable.
9. Javadocs for public APIs are useful.
10. In general, the fewer the function parameters the better.
11. Functions that return error codes make programs clearer by mixing error-handling logic with the main logic of the program.
12. Functions should return values rather than update output parameters.
13. You should avoid side-effects in your functions.
14. In test driven development, you may not begin writing production code until you have written a failing unit test.
15. A Junit test method should only have one assert statement.
16. Junit test methods are executed in a definite order so that test methods may depend on other test methods.
17. You should always use object-oriented class designs rather than data structure-oriented classes.
18. According to the Law of Demeter, a class's public API should not expose its internal structure.
19. According to the Single Responsibility Principle, a class should have one reason to change.
20. Clean class design usually results in several small classes instead of a few large classes.

## Multiple Choice (2 points each)

1. A Java classpath specification can contain:
- A. directories in which to find `.class` files on the filesystem
- B. `.jar` files that contain archives of directory trees that contain `.class` files.
- C. all of the above
- D. none of the above 

2. Which of the following is a **best** (not just legal) directory path to contain a class named `foo.bar.Baz`?
- A. `src/foo`
- B. `src/bar/foo`
- C. `src/main/java/foo/bar`
- D. All of the above are correct.

3. Agile software development processes minimize risk by
- A. hiring only the best software engineers.
- B. doing the higher-risk tasks in the last iteration.
- C. doing the higher-risk tasks first in each iteration.

4. The benefits of build automation include
- A. repeatable builds.
- B. documentation of build process.
- C. All of the above.

5. Which of the following guidelines apply to clean names in programs?
- A. All variable names should be short.
- B. All variable names should be long.
- C. The length of a variable name should be proportional to its scope.
- D. None of the above.

6. The stepdown rule (a.k.a. newspaper metaphor) in writing functions means placing
- A. longer functions before shorter functions.
- B. shorter functions before longer functions.
- C. higher-level functions above helper functions that implement the details.
- D. harder to understand functions before easier to understand functions.

7. Which of the following are good formatting practices?
- A. Vertical openness between concepts.
- B. Placing spaces on either side of assignment operators.
- C. Following the team's rules even if they differ from your own.
- D. All of the above.

8. Class size is measured in
- A. lines of code.
- B. number of methods.
- C. number of responsibilities.

9. Which of the following are indications of a class's number of responsibilities?
- A. The number of interfaces it implements.
- B. The vagueness of the class name.
- C. The number of words in the class name.
- D. All of the above.

10. Which of the following is a good strategy for cleaning up `switch` statements?
- A. Replacing them with `if-else` statements.
- B. Using `String`s for the switch variable.
- C. Replacing them with polymorphic classes.
- D. Including `default` clauses.

## Short Answer (5 points each)

1. Give a case where a comment is helpful.

2. What is the most important consideration in writing a function?

3. Tests should be F.I.R.S.T.  Explain each F.I.R.S.T. rule with one sentence or phrase.

4. Write a unit test method for the static method `public static int abs(int n)` in `java.lang.Math`, which returns the absolute value of an integer `n`.  Assume `java.lang.Math` has been imported.  Include an assert for each equivalence partition.


## Refactoring/Code Cleaning (5 points each)

Apply the principles of clean code to rewrite these code snippets the spaces below them.  If the code is in Java, write your solution in Java.  If the code is in Scala, write your solution in Scala.  If the code is valid in both Java and Scala, write your solution in Scala.



1.

```Java
public class Part {
  private String m_dsc; // The textual description
  public void set(String s) {
    m_dsc = s;
  }
}
```

2.

```Java
// returns 1 if the file named by s exists, 0 otherwise.
public int file(String s)
```

3.

```Java
// Check to see if the employee is eligible for full benefits
if ((employee.flags & HOURLY_FLAG) && (employee.age > 65))
```

4.

```Scala
def mean(xs: Seq[Double]): Double =
  if (xs.isEmpty)
    throw new ArithmeticException("mean of empty list undefined")
  else
    xs.sum / xs.length
```


## Design Analysis 1 (5 points each)

Given the following two designs for representing geometric shapes:

Design A:
```Java
public class Rectangle {
  public Point topLeft;
  public double height;
  public double width;
}
public class Circle {
  public Point center;
  public double radius;
}
public class Geometry {
  public double area(Object shape) throws NoSuchShapeException {
    if (shape instanceof Rectangle) {
      Rectangle r = (Rectangle)shape;
      return r.height * r.width;
    }
    else if (shape instanceof Circle) {
      Circle c = (Circle)shape;
      return Math.PI * c.radius * c.radius;
    }
    throw new NoSuchShapeException();
  }
}
```

Design B:
```Java
public interface Shape {
  public double area();
}
public class Rectangle implements Shape {
  private Point topLeft;
  private double height;
  private double width;
  public double area() { return height * width; }
}
public class Circle implements Shape {
  private Point center;
  private double radius;
  public double area() { return Math.PI * radius * radius; }
}
```
1. Give an advantage of Design A.

2. Give an advantage of Design B.

## Design Analysis 2 (5 points each)

Given:

```Java
public class RealBillingService {
  public Receipt chargeOrder(PizzaOrder order, CreditCard creditCard) {
    PaypalCreditCardProcessor processor = new PaypalCreditCardProcessor();
    // Card charging code ...
  }
}
```

1. List any "smells" in the design or the code.

2. State any principles or design patterns you would use to refactor the code.

3. Write an updated version of the code which applies the principles or design patterns in the previous question.  You may need to write additional classes or interfaces.  Only write code that is necessary to show the design, that is, dont' write detailed implementations for methods.

## Design Implementation (10 points)

Given the following UML class diagram, write a stubbed out `SalariedEmployee` class in Scala **using Scala idioms**.  Complete the constructor (if you need one) but don't give the code inside the curly braces of other methods.  Do not include anything you don't need in this class.


![](salaried-employee-uml.png)

## Algebraic Data Types (30 points)

Given the following polymorphic class hierarchy:

```Scala
trait Feline {
  def dinner: Food
}
final class Lion() extends Feline {
  def dinner: Food =
    Antelope
}
final class Tiger() extends Feline {
  def dinner: Food =
    TigerFood
}
final class Panther() extends Feline {
  def dinner: Food =
    Licorice
}
final class Cat(val favouriteFood: String) extends Feline {
  def dinner: Food =
    CatFood(favouriteFood)
}
```

Rewrite these classes as an algebraic data type such that you don't intend ever to add another subclass of `Feline`.  Also write the `Food` classes as an algebraic data type.
