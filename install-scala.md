---
layout: default
title: CS2340 - Install Scala
---

Scala is normally downloaded separately for each project using [sbt](sbt.html).

To intall Scala for system-wide use, follow the instructions at the bottom of the [Scala Downloads page](https://www.scala-lang.org/download/).
