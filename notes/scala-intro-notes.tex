\documentclass[letterpaper,12pt]{article}

\include{latex-common}

\title{Introduction to Scala}
\author{Christopher Simpkins}
\date{\today}

\begin{document}

\maketitle

\section{A Brief History of Scala}

The first thing to learn about Scala is its pronunciation -- {\it skah-lah}, not {\it scale-uh}.

Scala was created by \href{https://lampwww.epfl.ch/~odersky/}{Martin Odersky} beginning in 2001 and first released in 2003.  Martin Odersky was a Ph.D. student at ETH Zürich under \href{https://history.computer.org/pioneers/wirth.html}{Niklaus Wirth}, one of the most important figures in the history of programming languages (he created Pascal, Modula-2 and Oberon, among others).  Scala's design evolved from several experimental languages in which Martin Odersky combined object-oriented and functional programming.  Martin Odersky was also, with \href{http://homepages.inf.ed.ac.uk/wadler/}{Phillip Wadler} (another important figure in programming languages), the designer of GJ (Generic Java) and the chief implementer of the Java 5 compiler that introduced generics to the Java programming language.  Designing and implementing a new compiler for a very large established language is a big engineering effort with many stakeholders, so Marin Odersky has great credibility not only as a programming language theoretician but as a working software engineer.  These dual perspectives lead to many of the design decisions and evolution of the Scala programming language.  Scala tries to bring advances in programming language theory to working programmers, recognizing that they must deal with existing code bases and ecosystems (i.e., the JVM).  So, for example, Scala does not try to be a pure functional language like Haskell.  But Scala does rest on sound theoretical foundations and gives programmers the freedom to use any paradigm they wish and to combine them profitably.  The chief design goal of Scala is to combine object-oriented and functional programming.

\section{Notable Features of Scala}

Scala is a multi-paradigm language with many advanced features, some of which are listed below.

\begin{itemize}
    \item Scala is truly object-oriented, which means that every object in Scala is an instance of a class.  That means that you can invoke methods on values that appear to be of primitive types, e.g., {\tt 1.equals(1)}.  There are no primitive types as in Java or a distinction between static and instance contexts in classes.  Object-oriented programming is far simpler in Scala than in Java.  At the same time, Scala provides more advanced object-oriented features such as mixin-composition using traits which provides a form of multiple implementation inheritance while avoiding the \href{https://en.wikipedia.org/wiki/Multiple\_inheritance\#The\_diamond\_problem}{diamond inheritance problem}.
    \item Scala supports functional programming with first-class function values, real lambda expressions, and support for immutable data.  Note that a function value is itself a kind of object, just like everything in Scala.
    \item Scala has a rich static type system with type inference.  So you can provide type annotaions (usually recommended) just as you would in any other statically-typed language or leave off the types in most cases and have the feel of a ``dynamic'' scripting language.
    \item In addition to by-value parameters Scala supports by-name parameters which, together with other language features, allows you to create library functions that look like bult-in control structures.
    \item Scala is a JVM language with excellent Java interoperability.  You can use any Java object or library within Scala.  (Using Scala objects from Java doesn't always work well because Scala has many more features than Java.)
    \item Scala has a concise, pleasant syntax.
    \item Scala stands for ``scalable language.''  This scalability manifests itself in many ways: you can write single expressions in a REPL, small scripts, or multi-million line projects in Scala; you can run Scala on a single machine or take advantage of the excellent methods the Scala ecosystem provides for distributed computing, such as \href{https://akka.io/}{Akka} actors or \href{http://spark.apache.org/}{Spark}; you can start wtih simpler features of Scala and gradually learn the more advanced features.
\end{itemize}

The last two points above allow you to use Scala simply as a ``nicer Java.''  Indeed the two most common paths to Scala are: Java programmers wanting to ``level-up'' and functional programmers from ML or Haskell wanting to write functional programs while leveraging the huge Java ecosystem.  We will of course be following the first path and use Scala as a ramp to advanced functional programming.

\section{Running Scala}

In most professional settings Scala projects are tied to its build tool, \href{https://www.scala-sbt.org}{sbt}, and each Scala project has its own version of Scala installed (Scala is essentially a set of Java libraries).  Even the download instructions at \url{https://www.scala-lang.org/download/} direct new Scala users to use sbt.  However, there are many ways to run Scala.

\subsection{Scala REPL Using System-Wide Scala}

If you scroll past the sbt-based instructions on \url{https://www.scala-lang.org/download/} you can find ``other ways to install Scala'' for your operating system.  If you do this, then you can run an interactive Scala interpreter, also called a REPL (Read-Eval-Print-Loop), or run Scala scripts (discussed below).  After installing a system-wide Scala, execute {\tt scala} from your operating system's command line.  You'll get the Scala REPL, which will look something like this:

\begin{lstlisting}
$ scala
Welcome to Scala 2.12.8 (OpenJDK 64-Bit Server VM, Java 11.0.1).
Type in expressions for evaluation. Or try :help.

scala>
\end{lstlisting}

The Scala REPL is similar to REPLs for other languages, like Lisp or Python.  Every modern language has a REPL (Java finally got a REPL in version 9 but it's not as nice as the REPLs available for other languages).  In a REPL, you enter an expression (and by the way, all ``statements'' are expressions in Scala) and the REPL evaluates the expression and echoes its value.  Using a REPL is a great way to become familiar with the basics of a language and try things out.  Get started by entering a few simple arithmetic expressions.  Type CTRL-D or {\tt :quit} to exit the REPL.

\begin{lstlisting}
$ scala
Welcome to Scala 2.12.8 (OpenJDK 64-Bit Server VM, Java 11.0.1).
Type in expressions for evaluation. Or try :help.

scala> 1 + 1
res0: Int = 2

scala> :quit
$
\end{lstlisting}

\subsection{Scala Scripts Using System-Wide Scala}

You can run Scala code in a file with the {\tt scala} command in the same way that you can run code in so-called scripting languages.  In keeping with tradition, type this into an editor and save it in a file named {\tt hello.scala}.

\begin{lstlisting}[language=Scala]
println("Hello, world!")
\end{lstlisting}

At the command line run it like this:

\begin{lstlisting}[language=Scala]
$ scala hello.scala
Hello, world!
\end{lstlisting}

The {\tt scala} command compiles the file without saving the bytecode and runs the code line-by-line, conceptually just like the line-by-line execution of a script in an interpreted langauge like Python or Bash.  You may have noticed that the

\end{document}
