---
layout: default
title: CS2340 - Project
---

# General Project Information

Specific project requirements will be provided each semester on the semester schedule.  You will conduct your project as a Scrum development project and submit the following reports as project deliverables.  These reports must be in PDF format.  If you submit a report in a non-PDF format you will lose points.  If you submit a report in a proprietary format like Microsoft Word, you will receive ZERO points.

In each sprint report include your name and the names and IDs of all team members.

## Sprint Deliverables

At the end of each sprint (which we typically call milestones) you will submit three things:

1. Peer evaluation as an individual assignment on Canvas

    At the end of each sprint, each team member will individually submit an evaluation of their peers' contributions to the project. The submission should list each team member with a grade adjustment factor in increments of .05 which indicates the proportion of a team member's fair share of the work that the team member actually did. If each team member contributed their fair share, each team member would get 1.0. The sum of the project contribution factors for each team member must be equal to the number of team members. For example, if one team member contributed 110% of her fair share and gets 1.1, some other team member must must have done 90% of their fair share and gets 0.9 (or two team members get 0.95, etc). If any team member gets more than 1.1 or less than 0.9, please include a brief justification.

    Here's an example peer evaluation for a fictitious Team 0, with five members:
    
    ```sh
    Team 0
    
    1. Jay Chandrasekhar: 1.0
    2. Kevin Heffernan: 1.2 (Did all UI design and code and was a great Scrum Master)
    3. Steve Lemme: 0.9
    4. Paul Soter: 0.9
    5. Erik Stolhanske 1.0
    
    Total: 5.0
    ```
    
    If an individual did no work at all, give them a 0.0 and subtract 1.0 from your team's total.
    
    For each project grade for which there is a peer evaluation, each student's grade will be adjusted by multiplying the team's grade by a contribution factor.  For any adjustment to take place, each other team member must agree that some adjustment should occur.  The kindest of the adjustments will be applied.  For example, if 3 team members state that Paul Soter should get a 0.5 but one team member states that Paul Soter should get a 0.9, Paul Soter gets a 0.9.

2. Design documentation

    In your Git repository there should be high-level documentation of your software's architecture and design.  this documentation can be separate from the source code, or integrated into the source code in the form of Scaladoc comments.  Documentation may include UML diagrams and should discuss design patterns, dependency injection, and any other design elements your software employs.

3. Milestone tag in integration manager's Git repository, as described in the project description for your semester.

    There should be a README.md file explaining how to build, test and deploy your the application.  The README file should also state the location of the design documentation (docs folder, in Scaladocs, etc).
