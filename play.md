---
layout: default
title: Play! Framework
---

# Scala Play! Framework

The [Play! Framework Documentation](https://www.playframework.com/documentation/2.7.x/Home) contains all the information you need to get up to speed with Play!, but there are many ways to get started with a Play! project.  This guide shows you a one low-friction approach to starting a Play! project for this course.

These instructions are based on [IntelliJ IDEA's instructions for getting started with Play 2.x]().  Refer to those instructions fo more details.

## Step by Step Instructions For Setting Up a Play! Project

For your group project, theses steps (including Step 0) should be done by your integration manager.

0. (Optional) Create a project on [github.gatech.edu](https://github.gatech.edu/) and clone it on your local machine.

    - Choose a project name that's a valid Scala identifier. 
    - Initialize the project with a `README.md` and a `.gitignore` for PlayFramework so you can clone it. 
    - Clone it on your personal work computer.
    - The directory created by `git clone ...` will be your project directory.
    
1. In IntelliJ, follow the directions for [Creating a Play 2 template with Lightbend project starter](https://www.jetbrains.com/help/idea/getting-started-with-play-2-x.html#play_using_project_starte)

    - Using a Lightbend stater will give you the best starting point with example code.
    - We recommend using the "Play Scala Forms Example" as a starter.
        > Note: some of the Lightbend starter projects are out of date and may cause problems for you.  The "Play Scala Forms Example" is up to date and contains enough example code to help you understand all the core components of a Play! web application.
    - For the Project location, choose the directory created by `git clone ...` in Step 0 above (if applicable).
    
2. Since we're using [sbt](https://www.scala-sbt.org/), you can delete all the directories and files with `gradle` in their names.

3. Commit all the remaining files to your Git repository and push them to the server (if you initialized your project with a `.gitignore` for PlayFramework compiler output directories should be ignored).


