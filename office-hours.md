---
layout: default
title: CS2340 - Office Hours
---

# Office Hours

## Professor Simpkins's Office Hours located in CoC 133

- Mondays, 18:00 - 19:00
- Tuesdays, 15:00 - 17:00
- Other times by appt.

Any time my office door is open, feel free to walk in. Email me if you need to see me, even during office hours since I won't necessarily be in my office or the TA lab if I'm not expecting anyone.

## TA Office Hours in CoC 103 (past the vending machines)

This schedule is subject to change so be sure to double check this page before coming to office hours.

<iframe style="width: 90%; height: 1000px;" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTTMcM_xU5TSk7iDc1im7bun1MfmknwjyflRwqLLuarcl4pPIQmJsE5gSZVimHHSEWOet85oRp4XOa_/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
