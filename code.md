---
layout: default
title: CS2340 - Code
---

# Example Code

## <a name="basics" />Basics

- [values-variables.sc](code/values-variables.sc)
- [control-structures.sc](code/control-structures.sc)
- [function-basics.sc](code/function-basics.sc)
- [classes-objects.sc](code/classes-objects.sc)
- [functional-abstraction.sc](code/functional-abstraction.sc)
- [oop.sc](code/oop.sc)
- [case-classes.sc](code/case-classes.sc)

## <a name="advanced" />Advanced

- [generics.sc](code/generics.sc)
- [implicits.sc](code/implicits.sc)
- [for-expressions.sc](code/for-expressions.sc)


## <a name="fp" />Functional Programming

- [fp-basics.sc](code/fp-basics.sc)
- [fp-data-structures.sc](code/fp-data-structures.sc)
