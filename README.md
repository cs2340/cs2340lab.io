# cs2340.gitlab.io


## Generating Slides

First, you need [Pandoc 2](http://pandoc.org), Graphviz, [Java](https://openjdk.java.net/), and Python 3.X.  Then you need to install the following Python packages:

```bash
conda install pandocfilters pygraphviz
```
The filter `graphviz.py` is already in the `slides` directory.

```bash
for file in `ls *.md`; do pandoc $file -f markdown -t beamer --listings -H beamer-common.tex -F graphviz.py -o `(basename $file .md)`.pdf; done
```

Note that you'll need the latest version of LaTeX and particularly the caption package.  Update with:

```bash
sudo tlmgr update --self
sudo tlmgr update --all
```

